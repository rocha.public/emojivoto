# Encontro Cloud Native Apps
  
Encontro abordando Aplicações Cloud Native! :cloud:  

![Playlist](comunidade-playlist.png)
  
# O que caracteriza uma Aplicação Cloud Native?

E lá vamos nós! É hora de começar a desenvolver meu produto!  

- **Stateless**: Não armazeno nenhum tipo de estado.  
Não existem mais `@Sessions` ou `@Scopes`, é hora de otimizar os recursos.  
Dude, srry about that! :(  

![](angry-polar-bear.jpg)  


- Empacotado via **Container**: Tenho todas minhas dependências, executo em 'qualquer lugar'.  
A comunidade já se posiciona com relação aos containers https://www.opencontainers.org/  


- Arquitetura de **Micro-Serviços**: Coesão e desacoplamento.  
   
*A micro-service architecture is a software architecture style where complex applications are composed of **small**, **independent** processes communicating with each other using **language-agnostic APIs**. These application services are small, highly decoupled and focus on doing a small task.* - [Cisco Blog](https://blogs.cisco.com/cloud/the-journey-to-cloud-native)  


Mas... eu já tenho meu produto... e ele funciona!  

*There are several ways to **decompose** your application, and in general, there is not a specific right or wrong way. (...) Application composition **is as much an art as a science**.* - [CNCF Blog](https://www.cncf.io/blog/2017/05/15/developing-cloud-native-applications/)  


## É hora de Kubernetes

0. Antes de mais nada, ~DESAFIO~!  
Um dos módulos do sistema deixou de implementar um dos pré-requisitos fundamentais de uma Aplicação Cloud Native.  
Precisamos descobrir: 1 - Qual e módulo, e 2 - Qual o problema! :)  
  
Agora sim!   

Acessem: https://labs.play-with-k8s.com/  

**Dica Rápida**  
Use **ctrl + insert** para Copiar.  
E **shift + insert** para Colar.  

1. Inicialização do Cluster:  
```sh
kubeadm init --apiserver-advertise-address $(hostname -i) 
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
``` 

2. Após isso iremos aplicar duas configurações:  
```sh
kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous  
kubectl apply -f https://gitlab.com/rocha.public/emojivoto/raw/master/metrics.yaml
```  
Ok! Tudo Pronto! Hora de subir nossa aplicação!  

3. Um overview sobre `namespace`:  
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: emojivoto
```
  
4. Vamos dar um look no conjunto `Deployment` e `Service`.  
Iniciamos pelo `Deployment`:  

```yaml
---
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: web
  namespace: emojivoto
spec:
  selector:
    matchLabels:
      app: web-svc
  template:
    metadata:
      labels:
        app: web-svc
    spec:
      containers:
      - name: web-svc
        image: buoyantio/emojivoto-web:v5
        env:
        - name: WEB_PORT
          value: "80"
        ports:
        - containerPort: 80
          name: http
        resources:
          limits:
            cpu: 50m
            memory: 128Mi
          requests:
            cpu: 5m
            memory: 16Mi
status: {}
```
  
E agora vamos de `Service`:  
```yaml
---
apiVersion: v1
kind: Service     # TIPO DA CRIANCA (resource)
metadata:         # METADATA = INFORMACOES
  name: web-svc
  namespace: emojivoto
spec:             # SPEC = TECNICO
  type: NodePort  # Tipo do Service
  selector:
    app: web-svc  # RESPONSAVEL POR ENCONTRAR O TARGET 
  ports:
  - name: http    # Onde o servico estará respondendo?
    port: 80
    targetPort: 80
```
Vamos aplicá-los com: `kubectl apply -f https://gitlab.com/rocha.public/emojivoto/raw/master/emojivoto.yaml`  

5. Observaremos os logs do Robô e também do serviço de votos.  

6. Vamos configurar um serviço de auto-scale:  
`kubectl autoscale deployment web --cpu-percent=50 --min=1 --max=10 -n emojivoto`  

7. Fire in the hole!  
```sh
kubectl run -i --tty load-generator --image=busybox /bin/sh
<ENTER>
while true; do wget -q -O- http://web-svc.emojivoto/; done
```

Inclusive, podemos fazer um scale vertical em poucos segundos!  
`#showMeTheCode`  
